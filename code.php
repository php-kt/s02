<?php

//While Loop
// do not forget the iteration

function whileLoop() {
	$count = 5;
	while($count !==0) {
		echo $count.'<br/>';
		$count--;
	}
}

//Do-While Loop

function doWhileLoop() {
	$count = 20;
	do {
		echo $count.'<br/>';
		$count--;
	}while($count>0);
}

//For Loop

function forLoop() {
	for ($count = 0; $count <=20; $count++){
		echo $count.'<br/>';
	}
}

//continue and break

function modifiedForLoop(){
	for($count = 0; $count <=20; $count++){
		if($count % 2 === 0){
			continue;
		}
		echo $count.'<br/>';
		if($count>10){
			break;
		}
	}
}

//array manipulation

$studentNumbers = array ('2020-1923','2020-1924','2020-1925');

//version php 5.4 square brackets

$newStudentNumbers = ['2022-1111','2022-1112','2022-1113'];

//simple arrays

$tasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];

//php associative arrays

$gradePeriods = [
	'firstGrading' => 98.5,
	'secondGrading' => 94.3,
	'thirdGrading' => 90,
	'fourthGrading' => 97
];

//two-dimensional array

$heroes = [

	['Iron man', 'Thor', 'Hulk'],
	['Wolverine','Cyclops','Jean','Grey'],
	['Batman','Superman','Wonder woman']

];

//Array Iterative Method: foreach()

foreach($heroes as $hero){
	echo $hero;
}

//Array sorting

$computerBrands = ['Acer', 'Asus','Lenovo','Neo','Redfox','Gateway','Toshiba','Fujitsu'];

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);

//Other Array Functions

function SearchBrand($brands,$brand){
	return(in_array($brand, $brands))? "$brand is in the array."
	: "$brand is not in the array.";
};

$reversedGradePeriods =
	array_reverse($gradePeriods);



// Activity 1

function divisibleBy5(){
	for($count = 0; $count <=1000; $count++){
		if($count % 5 !== 0){
			continue;
		}
		echo $count.', ';
		if($count>1000){
			break;
		}
	}
}

//Activity 2

$array1 = [];

?>