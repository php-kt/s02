<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>s2: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
	<h1>Repetition Control Structures</h1>
	<?php whileLoop(); ?><br/><br/>
	<?php doWhileLoop(); ?><br/><br/>
	<?php forLoop(); ?><br/><br/>
	<?php modifiedForLoop();?><br/><br/>



	<h2>Associative Array</h2>

	<?php echo $gradePeriods -> firstGrading ?>

	<?php echo $gradePeriods['secondGrading']?>

	<ul>
		<?php foreach($gradePeriods as $period => $grade){
			?>

			<li>Grade in <?= $period ?> is <?= $grade ?></li>

		<?php } ?>

	</ul>

	<h2>2-Dimensional Array</h2>

		<?php echo $heroes[2][1] ?>

	<ul>
		<?php

			foreach($heroes as $team) {
				foreach($team as $member){
					?>
					<li><?php echo $member ?></li>

		<?php
				}
			}
		?>
	</ul>


	<h2>Sorting</h2>

	<pre>
		<?php print_r($sortedBrands); ?>
	</pre>

	<h2>Reverse Sorting</h2>
	<pre>
		<?php print_r($reverseSortedBrands); ?>
	</pre>

	<h2>Array Mutations (Append)</h2>

	<h3>push</h3>
	<?php array_push($computerBrands,'Apple') ?>
	<!-- print_r for arrays -->
	<pre>
		<?php print_r($computerBrands); ?>
	</pre>

	<h3>unshift</h3>
	<?php array_unshift($computerBrands,'Dell') ?>
	<!-- print_r for arrays -->
	<pre>
		<?php print_r($computerBrands); ?>
	</pre>

	<h2>Remove</h2>
	<?php array_pop($computerBrands); ?>
	<pre>
		<?php print_r($computerBrands); ?>
	</pre>

	<?php array_shift($computerBrands); ?>
	<pre>
		<?php print_r($computerBrands); ?>
	</pre>

	<h2>Count</h2>
	<pre>
		<?php echo count($computerBrands); ?>
	</pre>

	<p>
		<?php echo searchBrand($computerBrands, 'Asus'); ?>
	</p>

	<pre>
		<?php print_r($reverseSortedBrands); ?>
	</pre>



<!-- Activity 1 -->

<h1>Divisibles of Five</h1>
<?php divisibleBy5();?><br/>

<!-- Activity 2 -->

	<h1>Array Manipulation</h1>
	<?php array_push($array1,'John Smith') ?>
	<pre>
		<?php var_dump($array1); ?>
	</pre>
	<pre>
		<?php echo count($array1); ?>
	</pre>
	<?php array_push($array1,'Jane Smith') ?>
	<pre>
		<?php var_dump($array1); ?>
	</pre>
	<pre>
		<?php echo count($array1); ?>
	</pre>
	<?php array_shift($array1) ?>
	<pre>
		<?php var_dump($array1); ?>
	</pre>
	<pre>
		<?php echo count($array1); ?>
	</pre>

</body>
</html>